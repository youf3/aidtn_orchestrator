from flask import Flask
from flask_testing import TestCase
import os
import app
from test import OrchestratorTest
import unittest
import itertools
import time
import logging
import requests
import pandas
import json

requests_logger = logging.getLogger('urllib3')
requests_logger.setLevel(logging.WARN)


def get_files(num_files, disks):
    
    data = []
    iterator = itertools.cycle(disks)
    index = 0
    for i in range(num_files):        
        disk = iterator.__next__()
        data.append('{}/data{}'.format(disk, index))
        if disks.index(disk) == len(disks)-1: index+= 1
   
    return data

class TransferTest(TestCase):

    def create_app(self):
        app.app.config['TESTING'] = True
        app.app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
        app.app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        return app.app

    def setUp(self):
        if os.path.exists('orchestrator/test.db'):
            os.remove('orchestrator/test.db')
        from app import db
        db.create_all()

    def tearDown(self):
        os.remove('orchestrator/test.db')
    
    # def test_nuttcp_transfer(self):
    #     data = {
    #         'name' : 'r740xd1',
    #         'man_addr' : '165.124.33.174:5000',
    #         'data_addr' : '10.250.38.58',
    #         'username' : 'nobody',
    #         'interface' : 'vlan2038'
    #     }

    #     response = self.client.post('/DTN/',json=data)
    #     result = response.get_json()
    #     assert result == {'id' : 1}

    #     data = {
    #         'name' : 'r740xd2',
    #         'man_addr' : '165.124.33.175:5000',
    #         'data_addr' : '10.250.38.59',
    #         'username' : 'nobody',
    #         'interface' : 'vlan2038'
    #     }

    #     response = self.client.post('/DTN/',json=data)
    #     result = response.get_json()
    #     assert result == {'id' : 2}

    #     # data = {
    #     #     'disk0/data0' : {                
    #     #         'size' : '300M'
    #     #     }        
    #     # }

    #     # response = requests.post('http://165.124.33.175:5000/create_file/', json=data)
    #     # result = response.json()
    #     # assert result == 0 

    #     data = {            
    #         'srcfile' : ['disk0/data0'],
    #         'dstfile' : ['disk0/data0']
    #     }

    #     response = self.client.get('/ping/1/2')
    #     result = response.get_json()['latency']
    #     assert result is not None
        
    #     response = self.client.post('/transfer/nuttcp/2/1',json=data)
    #     result = response.get_json()
    #     assert result == {'result' : True, 'transfer' : 1}

    #     response = self.client.post('/wait/1')
    #     assert response.status_code == 200        

    #     response = self.client.get('/transfer/1')
    #     result = response.get_json()
    #     assert result['id'] == 1
    #     assert result['sender'] == 2
    #     assert result['receiver'] == 1
    #     assert result['transfer_size'] == 104857600
    #     assert result['num_workers'] == 1

    #     response = self.client.get('/transfer/nuttcp')
    #     result = response.get_json()
    #     assert result['1']        
    #     assert result['1']['sender'] == 2
    #     assert result['1']['receiver'] == 1
    #     assert result['1']['transfer_size'] == 104857600
    #     assert result['1']['num_workers'] == 1

    # def test_nuttcp_transfer_mem_to_disk(self):
    #     data = {
    #         'name' : 'r740xd1',
    #         'man_addr' : '165.124.33.174:5000',
    #         'data_addr' : '10.250.38.58',
    #         'username' : 'nobody',
    #         'interface' : 'vlan2038'
    #     }

    #     response = self.client.post('/DTN/',json=data)
    #     result = response.get_json()
    #     assert result == {'id' : 1}

    #     data = {
    #         'name' : 'r740xd2',
    #         'man_addr' : '165.124.33.175:5000',
    #         'data_addr' : '10.250.38.59',
    #         'username' : 'nobody',
    #         'interface' : 'vlan2038'
    #     }

    #     response = self.client.post('/DTN/',json=data)
    #     result = response.get_json()
    #     assert result == {'id' : 2}

    #     # data = {
    #     #     'disk0/data0' : {                
    #     #         'size' : '300M'
    #     #     }        
    #     # }

    #     # response = requests.post('http://165.124.33.175:5000/create_file/', json=data)
    #     # result = response.json()
    #     # assert result == 0 
        
    #     response = self.client.get('/ping/1/2')
    #     result = response.get_json()['latency']
    #     assert result is not None


    #     data = {            
    #         'srcfile' : [None],
    #         'dstfile' : [None],
    #         'duration' : 5
    #     }
        
    #     response = self.client.post('/transfer/nuttcp/2/1',json=data)
    #     result = response.get_json()
    #     assert result == {'result' : True, 'transfer' : 1}

    #     data = {            
    #         'srcfile' : ['disk0/data0'],
    #         'dstfile' : ['disk0/data0']
    #     }

    #     response = self.client.post('/transfer/nuttcp/2/1',json=data)
    #     result = response.get_json()
    #     assert result == {'result' : True, 'transfer' : 2}

    #     response = self.client.post('/wait/1')
    #     assert response.status_code == 200        

    #     response = self.client.post('/wait/2')
    #     assert response.status_code == 200        

    #     response = self.client.get('/transfer/1')
    #     result = response.get_json()
    #     assert result['id'] == 1
    #     assert result['sender'] == 2
    #     assert result['receiver'] == 1
    #     #assert result['transfer_size'] == 32212254720
    #     assert result['num_workers'] == 1

    #     response = self.client.get('/transfer/nuttcp')
    #     result = response.get_json()
    #     assert result['1']        
    #     assert result['1']['sender'] == 2
    #     assert result['1']['receiver'] == 1
    #     #assert result['transfer_size'] == 32212254720
    #     assert result['1']['num_workers'] == 1        

    #     response = self.client.get('/transfer/2')
    #     result = response.get_json()
    #     assert result['id'] == 2
    #     assert result['sender'] == 2
    #     assert result['receiver'] == 1
    #     #assert result['transfer_size'] == 32212254720
    #     assert result['num_workers'] == 1

    #     response = self.client.get('/transfer/nuttcp')
    #     result = response.get_json()
    #     assert result['2']        
    #     assert result['2']['sender'] == 2
    #     assert result['2']['receiver'] == 1
    #     #assert result['transfer_size'] == 32212254720
    #     assert result['2']['num_workers'] == 1



    # def test_nuttcp_timout(self):
    #     data = {
    #         'name' : 'r740xd1',
    #         'man_addr' : '165.124.33.174:5000',
    #         'data_addr' : '10.250.38.58',
    #         'username' : 'nobody',
    #         'interface' : 'vlan2038'
    #     }

    #     response = self.client.post('/DTN/',json=data)
    #     result = response.get_json()
    #     assert result == {'id' : 1}

    #     data = {
    #         'name' : 'r740xd2',
    #         'man_addr' : '165.124.33.175:5000',
    #         'data_addr' : '10.250.38.59',
    #         'username' : 'nobody',
    #         'interface' : 'vlan2038'
    #     }

    #     response = self.client.post('/DTN/',json=data)
    #     result = response.get_json()
    #     assert result == {'id' : 2}

    #     data = {
    #         'disk0/data0' : {                
    #             'size' : '300M'
    #         }        
    #     }

    #     response = requests.post('http://165.124.33.175:5000/create_file/', json=data)
    #     result = response.json()
    #     assert result == 0 

    #     data = {            
    #         'srcfile' : ['disk0/data0'],
    #         'dstfile' : ['disk0/data0'],
    #         'timeout' : 0
    #     }

    #     response = self.client.get('/ping/1/2')
    #     result = response.get_json()['latency']
    #     assert result is not None
        
    #     response = self.client.post('/transfer/nuttcp/2/1',json=data)
    #     result = response.get_json()
    #     assert result == {'result' : True, 'transfer' : 1}

    #     response = self.client.post('/wait/1')
    #     result = response.get_json()
    #     assert response.status_code == 200        
    #     assert result['failed'] == ['disk0/data0']

    #     response = self.client.get('/transfer/1')
    #     result = response.get_json()
    #     assert result['id'] == 1
    #     assert result['sender'] == 2
    #     assert result['receiver'] == 1
    #     #assert result['transfer_size'] == 32212254720
    #     assert result['num_workers'] == 1

    #     response = self.client.get('/transfer/nuttcp')
    #     result = response.get_json()
    #     assert result['1']        
    #     assert result['1']['sender'] == 2
    #     assert result['1']['receiver'] == 1
    #     #assert result['transfer_size'] == 32212254720
    #     assert result['1']['num_workers'] == 1

    # def test_multiple_nuttcp_transfer(self):
    #     data = {
    #         'name' : 'r740xd1',
    #         'man_addr' : '165.124.33.174:5000',
    #         'data_addr' : '10.250.38.58',
    #         'username' : 'nobody',
    #         'interface' : 'vlan2038'
    #     }

    #     response = self.client.post('/DTN/',json=data)
    #     result = response.get_json()
    #     assert result == {'id' : 1}

    #     data = {
    #         'name' : 'r740xd2',
    #         'man_addr' : '165.124.33.175:5000',
    #         'data_addr' : '10.250.38.59',
    #         'username' : 'nobody',
    #         'interface' : 'vlan2038'
    #     }

    #     response = self.client.post('/DTN/',json=data)
    #     result = response.get_json()
    #     assert result == {'id' : 2}

    #     num_files = 800
    #     disks = ['disk{}'.format(i) for i in range(0,7)]        
    #     fsize = '100M'
    #     files = get_files(num_files, disks)
    #     num_workers = 5

    #     # data = {i: {'size' : fsize} for i in files}

    #     # response = requests.post('http://165.124.33.175:5000/create_file/', json=data)
    #     # result = response.json()
    #     # assert result == 0        

    #     data = {
    #         'srcfile' : files,
    #         'dstfile' : files,
    #         'num_workers' : num_workers,
    #         'blocksize' : 64
    #     }

    #     response = self.client.post('/transfer/nuttcp/2/1',json=data)
    #     transfer_id = response.get_json()['transfer']
    #     assert transfer_id == 1

    #     time.sleep(3)
    #     data = {
    #         'num_workers' : 50
    #     }        

    #     response = self.client.post('/transfer/1/scale/',json=data)
    #     assert response.status_code == 200

    #     response = self.client.get('/transfer/1')
    #     assert response.status_code == 400

    #     response = self.client.get('/running')
    #     result = response.get_json()
    #     assert result == [1]

    #     response = self.client.post('/wait/1')
    #     result = response.get_json()
    #     assert result['result'] == True

    #     response = self.client.get('/transfer/1')
    #     result = response.get_json()
    #     assert result['id'] == 1
    #     assert result['sender'] == 2
    #     assert result['receiver'] == 1
    #     #assert result['transfer_size'] == 26
    #     assert result['num_workers'] == num_workers

    # def test_fio_transfer(self):
    #     data = {
    #         'name' : 'r740xd1',
    #         'man_addr' : '165.124.33.174:5000',
    #         'data_addr' : '10.250.38.58',
    #         'username' : 'nobody',
    #         'interface' : 'vlan2038'
    #     }

    #     response = self.client.post('/DTN/',json=data)
    #     result = response.get_json()
    #     assert result == {'id' : 1}

    #     data = {
    #         'name' : 'r740xd2',
    #         'man_addr' : '165.124.33.175:5000',
    #         'data_addr' : '10.250.38.59',
    #         'username' : 'nobody',
    #         'interface' : 'vlan2038'
    #     }

    #     response = self.client.post('/DTN/',json=data)
    #     result = response.get_json()
    #     assert result == {'id' : 2}

    #     data = {
    #         'disk0/data0' : {                
    #             'size' : '3000M'
    #         }        
    #     }

    #     response = requests.post('http://165.124.33.175:5000/create_file/', json=data)
    #     result = response.json()
    #     assert result == 0 

    #     data = {            
    #         'srcfile' : ['disk0/data0'],
    #         'dstfile' : ['disk0/data0']
    #     }

    #     response = self.client.get('/ping/1/2')
    #     result = response.get_json()['latency']
    #     assert result is not None
        
    #     response = self.client.post('/transfer/fio/2/2',json=data)
    #     result = response.get_json()
    #     assert result == {'result' : True, 'transfer' : 1}

    #     response = self.client.post('/wait/1')
    #     assert response.status_code == 200        

    #     response = self.client.get('/transfer/1')
    #     result = response.get_json()
    #     assert result['id'] == 1
    #     assert result['sender'] == 2
    #     assert result['receiver'] == 2
    #     assert result['transfer_size'] == 3145728000
    #     assert result['num_workers'] == 1

    #     response = self.client.get('/transfer/fio')
    #     result = response.get_json()
    #     assert result['1']        
    #     assert result['1']['sender'] == 2
    #     assert result['1']['receiver'] == 2
    #     assert result['1']['transfer_size'] == 3145728000
    #     assert result['1']['num_workers'] == 1

    
    def test_multiple_fio_transfer(self):
        data = {
            'name' : 'r740xd1',
            'man_addr' : '165.124.33.174:5000',
            'data_addr' : '10.250.38.58',
            'username' : 'nobody',
            'interface' : 'vlan2038'
        }

        response = self.client.post('/DTN/',json=data)
        result = response.get_json()
        assert result == {'id' : 1}

        data = {
            'name' : 'r740xd2',
            'man_addr' : '165.124.33.175:5000',
            'data_addr' : '10.250.38.59',
            'username' : 'nobody',
            'interface' : 'vlan2038'
        }

        response = self.client.post('/DTN/',json=data)
        result = response.get_json()
        assert result == {'id' : 2}

        num_files = 7
        disks = ['disk{}'.format(i) for i in range(0,7)]        
        fsize = '100G'
        files = get_files(num_files, disks)
        num_workers = 1

        data = {i: {'size' : fsize} for i in files}

        # response = requests.post('http://165.124.33.175:5000/create_file/', json=data)
        # result = response.json()
        # assert result == 0        

        data = {
            'srcfile' : files,
            'dstfile' : files,
            'num_workers' : num_workers,
            'blocksize' : 1024,
            'iomode' : 'read'
        }

        response = self.client.post('/transfer/fio/2/2',json=data)
        transfer_id = response.get_json()['transfer']
        assert transfer_id == 1

        time.sleep(30)
        data = {
            'num_workers' : 7
        }        

        response = self.client.post('/transfer/1/scale/',json=data)
        assert response.status_code == 200

        response = self.client.get('/transfer/1')
        assert response.status_code == 400

        response = self.client.get('/running')
        result = response.get_json()
        assert result == [1]

        response = self.client.post('/wait/1')
        result = response.get_json()
        assert result['result'] == True

        response = self.client.get('/transfer/1')
        result = response.get_json()
        assert result['id'] == 1
        assert result['sender'] == 2
        assert result['receiver'] == 2
        #assert result['transfer_size'] == 26
        assert result['num_workers'] == num_workers

    

    # def test_dynamic_msrsnyc_transfer(self):

    #     #df = pandas.read_csv('nvme_usage_daily.csv')
    #     nvme_values = [3.197330362868471, 3.0168408919609795, 2.8583068186783978, 2.8034660113779224, 2.704276099187711, 2.897180525896589, 3.7104560055566065, 3.5237964483668947, 4.504290381977851, 3.2684879335761963, 4.103909220244694, 4.825081494642074, 5.521934048005495, 5.210220256127304, 3.2898396907794467, 3.4557073794829822, 3.352555762985316, 6.480647115229032, 4.391203914612159, 4.931134815942074, 3.862770571310022, 3.540497098424229, 3.5938820962504145, 6.467792268672273, 4.816411636566738, 7.194502350629233, 6.320595045782891, 4.632849085678518, 4.398326199591487, 3.3927812406320523, 3.233709366315203, 4.214783407173402, 3.1407950231767487, 3.6950214768656884, 2.8605478406515585, 3.850321222367765, 3.1611607469760994, 2.9708021756772562, 2.5490974570233456, 2.520354544129346, 3.164281218100757, 3.1443649185895146, 2.6190678989705987, 2.8657306194369205, 3.160553897861758, 2.587335742504422, 2.5334780015015617, 2.6439205447642813, 2.6230443983848115, 3.672750041395706, 3.537621652432005, 3.5915924902142233, 3.3812496908240472, 3.979533769121941, 3.454386266820443, 4.396228472196103, 3.9712126778666335, 3.658497238160739, 3.1578739314466784, 3.130703522575341, 3.016237613011812, 2.8126312381183465, 3.040274576717262, 2.9738400833586383, 2.588424308827334, 2.641157208259161, 3.325746994961004, 2.4755476379153665, 3.131497816517088, 2.2837364036289047, 2.6684933557184274, 2.1778495698614937, 2.9009363571058286, 2.4018910628727084, 2.6319051322655707, 2.6861876712641632, 2.4967387551112945, 2.490255814956752, 2.5247076698871074, 2.22670691806629, 2.6102588070721118, 2.6488345695654476, 2.5311950108985597, 2.7203997373746103, 2.3038466825318555, 2.6457462188297933, 2.9232837030052896, 2.9554542495129725, 2.490749053824787, 2.616812033144799, 3.324680344407531, 2.9612409651160743, 2.55912661126696, 2.8847437075214715, 2.618160871457864, 3.4147625720296, 3.261577706333367, 2.667149804335641, 3.130266785354715, 2.658055271203985, 3.434726822563655, 3.3314058968663085, 3.6444697360068288, 2.9709163987470966, 2.7448619633050417, 2.70929260476929, 2.7243151986621648, 2.5395483244940578, 2.849744250390068, 2.919913236259011, 2.561959203531621, 2.476842074773629, 3.2292597957857554, 2.6043861105654913, 2.9681840280328275, 2.907611104295018, 2.583830952251574, 2.7458683444220107, 2.6079552519060525, 2.08655201459368, 3.142508514767052, 2.9326095814102295, 2.7860775768222545, 2.0466112535021224, 2.2750998613767544, 2.565843041607078, 1.8708829721676998, 2.0085404873291632, 2.2101280433806374, 2.1999331597358127, 1.5603147118545462, 2.009494089749889, 2.2776071467126444, 2.1551352071399545, 2.3474445282385306, 2.176597611396157, 2.16567418318038, 2.3885620246497923, 2.2538594052875074, 2.359925634965847, 2.3578808427116003, 1.9226796098212853, 2.0601516517132974, 1.901166622250888, 2.1749863127957085, 2.192618592478675, 2.21462706742168, 2.264603646019886, 2.7934252483397444, 2.054656117493907, 1.8136851815309367, 2.7012411326288306, 2.2867106927438248, 1.8068979369934137, 2.0478361807907053, 1.8892532421266626, 2.6172252151782116, 2.1887739788238467, 2.1540605739856544, 2.2134083092323946, 2.898025475812172, 2.5110386545635452, 4.014055661547731, 3.1510349325102887, 2.954184384195746, 2.088280144811666, 2.185012997205779, 1.7020094162458062, 2.665980508954943, 2.1856047901346316, 2.477482652355254, 2.215904220398427, 3.134774333017688, 3.3724624171644435, 3.0070247928879015, 2.982404510389034, 2.2972252201223977, 2.76492206586821, 2.6236881335439586, 3.040371120214073, 2.984931514635678, 2.3387851400082047, 2.2131421599015515, 1.6870555051737413, 2.401996350877501, 2.428515260102804, 2.9108999853314983, 2.686940157846187, 2.1697268996689343, 2.1860086770164298, 2.0516809116257724, 2.1553178439043226, 3.038160695147218, 3.8021933458479564, 2.2579415040603914, 2.6864075352139447, 2.226970338311144, 2.127907728987606, 3.871511143887489, 2.9271879437580366, 3.71840110501274, 2.9352545130231964, 3.2250875494254405, 3.586896693061298, 3.0438940106249266, 3.574485926666798, 3.2819586108772425, 2.9444528018612717, 3.3063128839755005, 3.411997260011218, 3.429967778056388, 3.685767599592032, 3.575864623090206, 3.608154738940603, 3.4082032196465746, 4.621049551379176, 4.541355897247899, 3.3426560544854222, 2.6620264295262888, 3.511245649936943, 3.4895644264490477, 3.3390576526717455, 3.3417165731853786, 3.561216889452601, 3.3048739104716685, 3.0680281286861115, 3.109481196608715, 3.713620809337629, 3.1268721007241824, 3.2967740979286537, 2.9926318903311784, 5.788936736511716, 6.778331044264559, 4.140166277747932, 4.16776366430548, 4.404825347297278, 3.6142747607859422, 4.367968499268176, 4.181948228049173, 3.6942204621489783, 3.455101413071042, 4.46916279863264, 3.6484063341159936, 4.045161279330165, 3.6445486645051743, 2.762852079213088, 3.288033012445818, 2.5869194455087445, 3.19315639599963, 3.386679619201122, 2.9738930747684704, 3.0871450882163614, 3.489118361149161, 3.411227596162546, 3.233457707264632, 2.670244243440639, 3.450489273995221, 4.02725290712523]
    #     iterator = iter(nvme_values)

    #     data = {
    #         'name' : 'r740xd1',
    #         'man_addr' : '165.124.33.174:5000',
    #         'data_addr' : '10.250.38.58',
    #         'username' : 'nobody',
    #         'interface' : 'vlan2038'
    #     }

    #     response = self.client.post('/DTN/',json=data)
    #     result = response.get_json()
    #     assert result == {'id' : 1}

    #     data = {
    #         'name' : 'r740xd2',
    #         'man_addr' : '165.124.33.175:5000',
    #         'data_addr' : '10.250.38.59',
    #         'username' : 'nobody',
    #         'interface' : 'vlan2038'
    #     }

    #     response = self.client.post('/DTN/',json=data)
    #     result = response.get_json()
    #     assert result == {'id' : 2}

    #     num_files = 8000
    #     disks = ['disk{}'.format(i) for i in range(0,8)]        
    #     fsize = '800M'
    #     files = get_files(num_files, disks)
    #     num_workers = 16
        
    #     data = {i: {'size' : fsize} for i in files}

    #     # response = requests.post('http://165.124.33.175:5000/create_file/', json=data)
    #     # result = response.json()
    #     # assert result == 0        

    #     data = {
    #         'srcfile' : files,
    #         'dstfile' : files,
    #         'num_workers' : num_workers,
    #         'blocksize' : 8192
    #     }

    #     response = self.client.post('/transfer/nuttcp/2/1',json=data)
    #     transfer_id = response.get_json()['transfer']
    #     assert transfer_id == 1

    #     # time.sleep(10)
    #     # data = {
    #     #     'blocksize' : 8192
    #     # }        

    #     # response = self.client.post('/transfer/1/scale/',json=data)
    #     # assert response.status_code == 200

    #     response = self.client.get('/transfer/1')
    #     assert response.status_code == 400

    #     response = self.client.get('/running')
    #     result = response.get_json()
    #     assert result == [1]

    #     while self.client.get('/check/1').get_json()['Unfinished'] != 0:

    #         parallel = int(next(iterator))

    #         data = {
    #             'address' : '/data/disk0',
    #             'file' : '/data/disk1',
    #             'parallel' : parallel
    #         }

    #         response = requests.post('http://165.124.33.175:5000/receiver/msrsync', json=data)
    #         result = response.json()
    #         assert result.pop('result') == True

    #         time.sleep(335)
    #         response = requests.get('http://{}/cleanup/msrsync'.format('165.124.33.175:5000'))            

    #     print('Finished')

    #     response = self.client.post('/wait/1')
    #     result = response.get_json()
    #     assert result['result'] == True

    #     response = self.client.get('/transfer/1')
    #     result = response.get_json()
    #     assert result['id'] == 1
    #     assert result['sender'] == 2
    #     assert result['receiver'] == 1
    #     #assert result['transfer_size'] == 26
    #     assert result['num_workers'] == num_workers

    # def test_poll(self):
    #     data = {
    #         'name' : 'r740xd1',
    #         'man_addr' : '165.124.33.174:5000',
    #         'data_addr' : '10.250.38.58',
    #         'username' : 'nobody',
    #         'interface' : 'vlan2038'
    #     }

    #     response = self.client.post('/DTN/',json=data)
    #     result = response.get_json()
    #     assert result == {'id' : 1}

    #     data = {
    #         'name' : 'r740xd2',
    #         'man_addr' : '165.124.33.175:5000',
    #         'data_addr' : '10.250.38.59',
    #         'username' : 'nobody',
    #         'interface' : 'vlan2038'
    #     }

    #     response = self.client.post('/DTN/',json=data)
    #     result = response.get_json()
    #     assert result == {'id' : 2}

    #     num_files = 80
    #     disks = ['disk{}'.format(i) for i in range(0,8)]        
    #     fsize = '100M'
    #     files = get_files(num_files, disks)
    #     num_workers = 40

    #     data = {i: {'size' : fsize} for i in files}

    #     response = requests.post('http://165.124.33.175:5000/create_file/', json=data)
    #     result = response.json()
    #     assert result == 0        

    #     data = {
    #         'srcfile' : files,
    #         'dstfile' : files,
    #         'num_workers' : num_workers,
    #         'blocksize' : 64
    #     }

    #     response = self.client.post('/transfer/nuttcp/2/1',json=data)
    #     transfer_id = response.get_json()['transfer']
    #     assert transfer_id == 1        
        
    #     response = self.client.get('/transfer/1')
    #     assert response.status_code == 400

    #     response = self.client.get('/check/1')
    #     result = response.get_json()
    #     assert 'Finished' in result
    #     assert 'Unfinished' in result

    #     response = self.client.get('/running')
    #     result = response.get_json()
    #     assert result == [1]

    #     response = self.client.post('/wait/1')
    #     result = response.get_json()
    #     assert result['result'] == True

    #     response = self.client.get('/check/1')
    #     result = response.get_json()
    #     assert result['Unfinished'] == 0
        
    #     response = self.client.get('/transfer/1')
    #     result = response.get_json()
    #     assert result['id'] == 1
    #     assert result['sender'] == 2
    #     assert result['receiver'] == 1
    #     #assert result['transfer_size'] == 26
    #     assert result['num_workers'] == num_workers

    # def test_msrsync(self):
    #     data = {            
    #         'address' : '/data/disk0',
    #         'file' : '/data/disk1/msrsync'
    #     }

    #     response = requests.post('http://165.124.33.175:5000/receiver/msrsync', json=data)
    #     result = response.json()
    #     assert result.pop('result') == True        

    #     time.sleep(1)

    #     response = requests.get('http://165.124.33.175:5000/cleanup/msrsync')
    #     assert response.status_code == 200


    #     response = requests.get('http://165.124.33.175:5000/msrsync/poll', json={})        
    #     assert response.status_code == 200

    # def test_msrsync_and_nuttcp(self):

    #     data = {
    #         'name' : 'r740xd1',
    #         'man_addr' : '165.124.33.174:5000',
    #         'data_addr' : '10.250.38.58',
    #         'username' : 'nobody',
    #         'interface' : 'vlan2038'
    #     }

    #     response = self.client.post('/DTN/',json=data)
    #     result = response.get_json()
    #     assert result == {'id' : 1}

    #     data = {
    #         'name' : 'r740xd2',
    #         'man_addr' : '165.124.33.175:5000',
    #         'data_addr' : '10.250.38.59',
    #         'username' : 'nobody',
    #         'interface' : 'vlan2038'
    #     }

    #     response = self.client.post('/DTN/',json=data)
    #     result = response.get_json()
    #     assert result == {'id' : 2}

    #     num_files = 4
    #     data = {}
        
    #     # for i in range(num_files):
    #     #     data['/data/disk0/data{}'.format(i)] = {'size' : '100GB'}
    #     # response = requests.post('http://165.124.33.175:5000/create_file/',json=data)
    #     # result = response.json()

    #     data = {            
    #         'address' : '/data/disk0',
    #         'file' : '/data/disk1/msrsync'
    #     }

    #     response = requests.post('http://165.124.33.175:5000/receiver/msrsync', json=data)
    #     result = response.json()
    #     assert result.pop('result') == True

    #     files = ['/data/disk0/data0', '/data/disk0/data1', '/data/disk0/data2', '/data/disk0/data3']

    #     data = {
    #         'srcfile' : files,
    #         'dstfile' : files,
    #         'num_workers' : 4,
    #         'blocksize' : 64
    #     }

    #     response = self.client.post('/transfer/nuttcp/2/1',json=data)
    #     transfer_id = response.get_json()['transfer']
    #     assert transfer_id == 1

    #     response = self.client.post('/wait/1')
    #     result = response.get_json()
    #     assert result['result'] == True  

    #     response = requests.get('http://165.124.33.175:5000/msrsync/poll', json={})        
    #     assert response.status_code == 200


if __name__ == '__main__':
    unittest.main(verbosity=2)