from flask import Flask
from flask_testing import TestCase
import os
import app
from test import OrchestratorTest
import unittest
import itertools
import time
import logging
import requests
import pandas
import json

requests_logger = logging.getLogger('urllib3')
requests_logger.setLevel(logging.WARN)


def get_files(num_files, disks):
    
    data = []
    iterator = itertools.cycle(disks)
    index = 0
    for i in range(num_files):        
        disk = iterator.__next__()
        data.append('{}/data{}'.format(disk, index))
        if disks.index(disk) == len(disks)-1: index+= 1
   
    return data

class TransferTest(TestCase):

    def create_app(self):
        app.app.config['TESTING'] = True
        app.app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
        app.app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        return app.app

    def setUp(self):
        if os.path.exists('orchestrator/test.db'):
            os.remove('orchestrator/test.db')
        from app import db
        db.create_all()

    def tearDown(self):
        os.remove('orchestrator/test.db')
    
    
    def test_multiple_nuttcp_transfer(self):
        data = {
            'name' : '24NVMeDTN1',
            'man_addr' : '165.124.33.188:5000',
            'data_addr' : '10.250.38.92',
            'username' : 'nobody',
            'interface' : 'vlan2038'
        }

        response = self.client.post('/DTN/',json=data)
        result = response.get_json()
        assert result == {'id' : 1}

        data = {
            'name' : '24NVMeDTN2',
            'man_addr' : '165.124.33.176:5000',
            'data_addr' : '10.250.38.93',
            'username' : 'nobody',
            'interface' : 'p1p1'
        }

        response = self.client.post('/DTN/',json=data)
        result = response.get_json()
        assert result == {'id' : 2}

        result = requests.get('http://165.124.33.188:5000/cleanup/nuttcp').json()
        result = requests.get('http://165.124.33.176:5000/cleanup/nuttcp').json()

        num_workers = 8    
        result = requests.get('http://165.124.33.188:5000/files/').json()
        files = [i['name'] for i in result if i['type'] == 'file'][:8]

        data = {
            'srcfile' : files,
            'dstfile' : files,
            'num_workers' : num_workers,
            'blocksize' : 8192,
            'zerocopy' : True
        }

        response = self.client.post('/transfer/nuttcp/1/2',json=data)
        transfer_id = response.get_json()['transfer']
        assert transfer_id == 1

        # time.sleep(3)
        # data = {
        #     'num_workers' : 50
        # }        

        # response = self.client.post('/transfer/1/scale/',json=data)
        # assert response.status_code == 200

        # response = self.client.get('/transfer/1')
        # assert response.status_code == 400

        response = self.client.get('/running')
        result = response.get_json()
        assert result == [1]

        response = self.client.post('/wait/1')
        result = response.get_json()
        assert result['result'] == True

        response = self.client.get('/transfer/1')
        result = response.get_json()
        assert result['id'] == 1
        assert result['sender'] == 1
        assert result['receiver'] == 2
        #assert result['transfer_size'] == 26
        assert result['num_workers'] == 8
   

if __name__ == '__main__':
    unittest.main(verbosity=2)