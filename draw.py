import matplotlib
import matplotlib.pyplot as plt
import requests
import logging
import json
import copy
from prometheus_http_client import Prometheus
from http.client import HTTPException
import pandas as pd
import numpy as np
from cycler import cycler

AVG_INT = 15
STEP = 5
MAX_RES = 11000
PRED_VARS = ['CPU', 'Storage', 'network_throughput', 'Storage_throughput']
MAX_PRED_VARS = [ 'CPU_count', 'Storage_count', 'NIC_speed']
WORKER_OPTS = ['num_workers', 'worker_type_id']

def send_query(query, start, end, step):
    
    prometheus = Prometheus()
    prometheus.url = 'http://165.124.33.158:9091'

    res = prometheus.query_rang(metric=query, start=start, end=end, step=step)    
    return res

def prettify_header(metric):
    if 'instance' in metric: del metric['instance']
    if 'job' in metric: del metric['job']
    if 'mode' in metric: del metric['mode']
    if '__name__' in metric: del metric['__name__']

    if len(metric) > 1 : raise Exception('too many metric labels')
    else:
        return next(iter(metric.keys()))

def export_data_for_prophet(receiver, interface, start_time, end_time, **params):
    query = ('label_replace((sum by (job)(irate(node_network_receive_bytes_total{{job="{0}", device="{2}"}}[{1}s]) * 8)),"network_throughput", "$0", "job", "(.+)") '
    'or label_replace(sum by (job) (1 - irate(node_cpu_seconds_total{{job="{0}",mode="idle"}}[{1}s])),"CPU", "$0", "job", "(.+)") '
    #'or label_replace(node_memory_Active_bytes{{job="{0}"}}, "Memory_used", "$0", "job", "(.+)") '
    'or label_replace(sum by (job)(irate(node_disk_written_bytes_total{{job="{0}", device=~"nvme([0-9]|1[0-5])n1"}}[{1}s])),"Storage_throughput", "$0", "job", "(.+)") '
    'or label_replace(sum by (job)(irate(node_disk_io_time_seconds_total{{job="{0}", device=~"nvme([0-9]|1[0-5])n1"}}[{1}s])),"Storage", "$0", "job", "(.+)") '
    'or label_replace(count(node_cpu_seconds_total{{mode="idle",job="{0}"}}) without (cpu,mode, instance),"CPU_count", "$0", "job", "(.+)") '
    #'or label_replace(node_memory_MemTotal_bytes{{job="{0}"}}, "Memory_total", "$0", "job", "(.+)") '
    #'or label_replace(count by (job)(node_disk_written_bytes_total{{job="r740xd2", device=~"nvme([0-9]|1[0-5])n1"}}),"Storage_count", "$0", "job", "(.+)")'
    'or label_replace(count by (job)(node_disk_io_time_seconds_total{{job="r740xd2", device=~"nvme([0-9]|1[0-5])n1"}}),"Storage_count", "$0", "job", "(.+)")'
    'or label_replace(sum by (job)(node_network_speed_bytes{{job="{0}", device="{2}"}} * 8), "NIC_speed", "$0", "job", "(.+)")'
    '').format(receiver, AVG_INT, interface)
    rows = []
    dataset = None
    

    while end_time > start_time:        
        data_in_period = None
        max_ts = start_time + (STEP * MAX_RES)
        next_hop_ts = end_time if max_ts > end_time else max_ts
        logging.debug('Getting data for {} : {}'.format(start_time, end_time))
        res = send_query(query, start_time, next_hop_ts, STEP)
        if '401 Authorization Required' in res: raise HTTPException(res)
        response = json.loads(res)
        if response['status'] != 'success': raise Exception('Failed to query Prometheus server')
        
        for result in response['data']['result']:
            result['metric'] = prettify_header(result['metric'])            
            df = pd.DataFrame(data=result['values'], columns = ['Time', result['metric']], dtype=float)            
            df['Time'] = pd.to_datetime(df['Time'], unit='s')
            df.set_index('Time', inplace=True)

            data_in_period = df if data_in_period is None else data_in_period.join(df)
        
        dataset = data_in_period if dataset is None else dataset.append(data_in_period)
        start_time = next_hop_ts
        
    return dataset.loc[:, PRED_VARS], dataset.loc[:, MAX_PRED_VARS]


matplotlib.rcParams.update({'font.size': 22})
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42


default_cycler = (cycler(color=['r', 'g', 'b', 'y', 'c', 'm', 'k']) +
                  cycler(linestyle=['-', '--', ':', '-.', '-', '--', ':']) + 
                  cycler(marker=['o', 'v', '^', '*', 'X', 'D', 'P']) + 
                  cycler(markersize=[10,10,10,10,10,10,10]))


fig, ax = plt.subplots(figsize=(14, 8))
ax.set_prop_cycle(default_cycler)
#axes[1].set_prop_cycle(default_cycler)

transfers = {    
    2395: {'scheme' : 'Starting', 'workers' : [40, 40, 40, 40]},
    2399: {'scheme' :'Starting + Dynamic', 'workers' : [40, 40, 40, 44]},
    2397: {'scheme' :'Dynamic scaling', 'workers' : [8, 23, 27, 31, 31]},
    2396: {'scheme' :'No optimization', 'workers' : [8] * 20}}

for transfer, val in transfers.items():
    response = requests.get('{}/transfer/{}'.format('http://165.124.33.174:7000', transfer))
    result = response.json()
    ts_data, _ = export_data_for_prophet('r740xd1', 'vlan2038', result['start_time'], result['end_time'])
    
    ts_data['elapsed'] = (ts_data.index - ts_data.index[0]) / np.timedelta64(1, 's')

    ax.plot(ts_data['elapsed'], ts_data['network_throughput'] / 1000000000, label=val['scheme'], lw=3)
    workers = np.array([[i] * 13 for i in val['workers']]).flatten()[:ts_data.shape[0]]
    
    #ax.set_prop_cycle(default_cycler)
    #ax.plot(ts_data['elapsed'], workers, lw=3, label=val['scheme'])

ax.text(0, 82, 'N=40')
ax.text(15, 29, 'N=8')
ax.text(70, 65, 'N=23')
ax.text(140, 70, 'N=27')
ax.text(200, 75, 'N=31')

ax.yaxis.grid(True, linestyle='-', which='major', color='lightgrey', 
alpha=0.5)
ax.xaxis.grid(True, linestyle='-', which='major', color='lightgrey', 
alpha=0.5)

ax.set_xlabel('Elapsed Time (s)')
ax.set_ylabel('Throughput (Gb/s)')
#axes[1].set_ylabel('Number of workers')

plt.ylim(bottom=0)
ax.legend()

ax.yaxis.grid(True, linestyle='-', which='major', color='lightgrey', 
alpha=0.5)
ax.xaxis.grid(True, linestyle='-', which='major', color='lightgrey', 
alpha=0.5)

plt.tight_layout()
# plt.show()
plt.savefig('test.pdf')
plt.close()

#--------------------------------

fig, ax = plt.subplots(figsize=(14, 8))
ax.set_prop_cycle(default_cycler)
#axes[1].set_prop_cycle(default_cycler)

transfers = {    
    2400: {'scheme' :'Dynamic + 8M BS'},
    2401: {'scheme' :'Dynamic + 4K BS'},
    2405: {'scheme' :'Starting (64K)'},
    2404: {'scheme' :'Starting (64K) + Dynamic'}}

for transfer, val in transfers.items():
    response = requests.get('{}/transfer/{}'.format('http://165.124.33.174:7000', transfer))
    result = response.json()
    ts_data, _ = export_data_for_prophet('r740xd1', 'vlan2038', result['start_time'], result['end_time'])
    
    ts_data['elapsed'] = (ts_data.index - ts_data.index[0]) / np.timedelta64(1, 's')

    ax.plot(ts_data['elapsed'], ts_data['Storage_throughput'] * 8 / 1000000000, label=val['scheme'], lw=3)    
    
    #ax.set_prop_cycle(default_cycler)
    #ax.plot(ts_data['elapsed'], workers, lw=3, label=val['scheme'])

ax.text(0, 72, 'BS=8M' ,color = 'r')
ax.text(30, 74, 'BS=4M', color = 'r')
ax.text(65, 80, 'BS=2M', color = 'r')
ax.text(8, 20, 'BS=4K', color = 'g' )
ax.text(35, 32, 'BS=8K', color = 'g' )
ax.text(63, 50, 'BS=16K', color = 'g' )
ax.text(90, 70, 'BS=32K', color = 'g' )
ax.text(130, 82, 'BS=64K', color = 'g' )
ax.text(0, 92, 'BS=64K', color = 'y' )

ax.yaxis.grid(True, linestyle='-', which='major', color='lightgrey', 
alpha=0.5)
ax.xaxis.grid(True, linestyle='-', which='major', color='lightgrey', 
alpha=0.5)

ax.set_xlabel('Elapsed Time (s)')
ax.set_ylabel('Throughput (Gb/s)')
#axes[1].set_ylabel('Number of workers')

plt.ylim(bottom=0)
ax.legend()

ax.yaxis.grid(True, linestyle='-', which='major', color='lightgrey', 
alpha=0.5)
ax.xaxis.grid(True, linestyle='-', which='major', color='lightgrey', 
alpha=0.5)

plt.tight_layout()
# plt.show()
plt.savefig('test2.pdf')