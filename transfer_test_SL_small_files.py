import requests
import itertools
import traceback
import time
import random

def deleteall(disks):
    print('deleting all files')    
    
    response = requests.delete('http://{}/file/*'.format('165.124.33.174:5000'))
    result = response.status_code
    response = requests.delete('http://{}/file/*'.format('165.124.33.175:5000') )
    result = response.status_code

    response = requests.get('http://{}/trim'.format('165.124.33.174:5000'))
    result = response.json()['returncode']
    assert result == 0

    response = requests.get('http://{}/trim'.format('165.124.33.175:5000'))
    result = response.json()['returncode']
    assert result == 0

    time.sleep(5)


def test_create_files(num_files, disks, size):
    
    data = {}    
    iterator = itertools.cycle(disks)
    index = 0 
    for i in range(num_files):        
        disk = iterator.__next__()
        data['{}/data{}'.format(disk, index)] = {'size' : '{}'.format(size)}        
        if disks.index(disk) == len(disks)-1: index+= 1

    deleteall(disks)
    print('creating %s files' % num_files)
    response = requests.post('http://{}/create_file/'.format('165.124.33.175:5000'),json=data)
    result = response.json()
    if response.status_code != 200: raise Exception('failed to create files')
    return data


def test_random_small_Transfer():
    # data = {
    #     'name' : 'r740xd1',
    #     'man_addr' : '165.124.33.174:5000',
    #     'data_addr' : '10.250.38.58',
    #     'username' : 'nobody'
    # }
    # response = requests.post('http://{}/DTN/'.format('165.124.33.174:7000'), json=data)    
    
    # result = response.get_json()
    # assert result == {'id' : 1}
    # data = {
    #     'name' : 'r740xd2',
    #     'man_addr' : '165.124.33.175:5000',
    #     'data_addr' : '10.250.38.59',
    #     'username' : 'nobody'
    # }
    # response = requests.post('http://{}/DTN/'.format('165.124.33.174:7000'), json=data)    
    
    # result = response.get_json()
    # assert result == {'id' : 2}

    disks = ['disk{}'.format(i) for i in range(0,8)]    

    # for num_files in range(80,8000, 80):
        # fsize = '{}M'.format(int(80*1024*8/num_files))
        # files = test_create_files(num_files, disks, fsize)
        
    for j in range(0, 1000):
        num_files = random.randrange(10,3000) * 8
        fsize = '{}M'.format(int(50*1024*8/num_files))
        files = test_create_files(num_files, disks, fsize)

        files = [i for i in files]
        
        #for num_threads in range(1, 80):
        for num_threads in random.sample(list(range(1,300)), k=10):
            print('transfer using %s threads' % num_threads)
            time.sleep(30)
            data = {
                'srcfile' : files,
                'dstfile' : files,
                'num_workers' : num_threads
            }                       

            try:
                response = requests.post('http://{}/transfer/nuttcp/2/1'.format('165.124.33.174:7000'),json=data)
                result = response.json()
                assert result['result'] == True
                transfer_id = result['transfer']

                response = requests.get('http://{}/transfer/{}'.format('165.124.33.174:7000', transfer_id))
                result = response.json()
                assert result['id'] == transfer_id
                assert result['sender'] == 2
                assert result['receiver'] == 1        
                start_time = result['start_time']
                end_time = result['end_time']
                size = result['transfer_size']
                num_worker = result['num_workers']
                num_files = result['num_files']
                latency = result['latency']

                with open('test_result.log', 'a') as fh:
                    fh.write('{}, {}, {}, {}, {}, {}, {}\n'.format(transfer_id, start_time, end_time, size, num_worker, num_files, latency))
                    fh.flush()

            except Exception as e:
                pass

def test_large_Transfer():   
    
    num_files = 8000
    disks = ['disk{}'.format(i) for i in range(0,8)]
    #fsize = '{}M'.format(int(100*1024*8/num_files))
    fsize = '100M'
    files = test_create_files(num_files, disks, fsize)

    for num_threads in range(50, 200):       

        files = [i for i in files]

        for i in range(1, 5):            
            print('transfer using %s threads' % num_threads)
            time.sleep(30)
            data = {
                'srcfile' : files,
                'dstfile' : files,
                'num_workers' : num_threads
            }

            try:
                response = requests.post('http://{}/transfer/nuttcp/2/1'.format('165.124.33.174:7000'),json=data)
                result = response.json()
                assert result['result'] == True
                transfer_id = result['transfer']

                response = requests.post('http://{}/wait/{}'.format('165.124.33.174:7000', transfer_id),json=data)
                result = response.json()
                assert result['result'] == True

                response = requests.get('http://{}/transfer/{}'.format('165.124.33.174:7000', transfer_id))
                result = response.json()
                assert result['id'] == transfer_id
                assert result['sender'] == 2
                assert result['receiver'] == 1        
                start_time = result['start_time']
                end_time = result['end_time']
                size = result['transfer_size']
                num_worker = result['num_workers']
                num_files = result['num_files']
                latency = result['latency']

                with open('test_result.log', 'a') as fh:
                    fh.write('{}, {}, {}, {}, {}, {}, {}\n'.format(transfer_id, start_time, end_time, size, num_worker, num_files, latency))
                    fh.flush()

            except Exception as e:
                print(e)
                pass

def test_scale():   
    
    num_files = 16000
    disks = ['disk{}'.format(i) for i in range(0,8)]
    #fsize = '{}M'.format(int(100*1024*8/num_files))
    fsize = '100M'
    files = test_create_files(num_files, disks, fsize)
    
    files = [i for i in files]

    num_threads = 8
    print('transfer using %s threads' % num_threads)

    data = {
        'srcfile' : files,
        'dstfile' : files,
        'num_workers' : num_threads
    }
    
    response = requests.post('http://{}/transfer/nuttcp/2/1'.format('165.124.33.174:7000'),json=data)
    result = response.json()
    assert result['result'] == True
    transfer_id = result['transfer']
    print('transfer id : %s' % transfer_id)
    time.sleep(30)

    data = {'num_workers' : 30}
    response = requests.post('http://{}/transfer/{}/scale/'.format('165.124.33.174:7000', transfer_id),json=data)
    assert response.status_code == 200

    response = requests.post('http://{}/wait/{}'.format('165.124.33.174:7000', transfer_id),json=data)
    result = response.json()
    assert result['result'] == True

    requests.delete('http://{}/transfer/{}'.format('165.124.33.174:7000', transfer_id))
    assert requests.status_codes == 200

def test_add_DTN():
    data = {
        'name' : 'r740xd1',
        'man_addr' : '165.124.33.174:5000',
        'data_addr' : '10.250.38.58',
        'username' : 'nobody'
    }
    response = requests.post('http://{}/DTN/'.format('localhost:5000'), json=data)    
    result = response.json()
    assert result == {'id' : 1}

    data = {
        'name' : 'r740xd2',
        'man_addr' : '165.124.33.175:5000',
        'data_addr' : '10.250.38.59',
        'username' : 'nobody'
    }
    response = requests.post('http://{}/DTN/'.format('localhost:5000'), json=data)
    result = response.json()
    assert result == {'id' : 2}

def test_local_transfer():
    data = {
        'srcfile' : ['disk0/data'],
        'dstfile' : ['disk0/data'],
        'num_threads' : 1
    }

    response = requests.post('http://{}/transfer/nuttcp/2/1'.format('localhost:5000'),json=data)
    result = response.json()

# def test_remove_transfer():
#     for i in range(801,802):
#         response = requests.delete('http://{}/transfer/{}'.format('165.124.33.174:7000', i))
#         result = response.json()
#         assert result['id'] == i

#test_local_transfer()
#test_add_DTN()
#test_get_DTN()
# test_remove_transfer()

#test_random_small_Transfer()
#test_large_Transfer()
test_scale()